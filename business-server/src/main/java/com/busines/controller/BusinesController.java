package com.busines.controller;

import com.common.model.request.CreateOrderRequestModel;
import com.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/6/10 下午3:25
 */
@Slf4j
@RequestMapping("/order")
@RestController
public class BusinesController {

    @Reference(version = "1.0.0")
    private OrderService orderService;

    @PostMapping
    public Boolean createOrder(@RequestBody CreateOrderRequestModel model){
        log.info("BusinesController.createOrder parms = {}",model.toString());
       return this.orderService.createOrder(model);
    }
}
