package com.busines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class BusinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinesApplication.class, args);
	}

}
