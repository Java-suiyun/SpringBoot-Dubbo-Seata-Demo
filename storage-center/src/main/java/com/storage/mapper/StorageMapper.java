package com.storage.mapper;

import com.storage.model.Storage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface StorageMapper extends BaseMapper<Storage> {

}
