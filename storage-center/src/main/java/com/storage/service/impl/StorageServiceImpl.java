package com.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.storage.model.Storage;
import com.storage.mapper.StorageMapper;
import com.storage.service.StorageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
@Service(version = "1.0.0")
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements StorageService {

    @Override
    public Boolean decreaseStorage(Storage commodityDTO) {

        Storage storage = this.baseMapper.selectOne(new QueryWrapper<Storage>()
                .eq("commodity_code", commodityDTO.getCommodityCode())
        );
        if (storage != null){
            storage.setCount(storage.getCount() - commodityDTO.getCount());
        }

        this.baseMapper.updateById(storage);
        return Boolean.TRUE;
    }
}
