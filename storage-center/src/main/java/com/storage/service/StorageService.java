package com.storage.service;

import com.storage.model.Storage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface StorageService extends IService<Storage> {

    /**
     * 扣减库存
     */
    Boolean decreaseStorage(Storage commodityDTO);
}
