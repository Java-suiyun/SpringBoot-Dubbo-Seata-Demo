# Seata-Demo

#### 介绍
分布式事务(dubbo+zookeeper)


#### 使用说明

1.  执行db下sql文件
2.  启动zk
3.  启动Seata Server
4.  启动account,order,storage,busines
5.  访问http://localhost:7013/order 
{
    "userId":"1",
    "commodityCode":"C201901140001",
    "name":"fan",
    "count":2,
    "amount":"100"
}
6. test the rollback request
if (true) {
    throw new RuntimeException("测试抛异常后，分布式事务回滚！");
}