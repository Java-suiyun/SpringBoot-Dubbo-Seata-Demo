package com.account.mapper;

import com.account.model.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface AccountMapper extends BaseMapper<Account> {

}
