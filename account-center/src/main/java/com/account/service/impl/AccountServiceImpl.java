package com.account.service.impl;

import com.account.mapper.AccountMapper;
import com.account.model.Account;
import com.account.service.AccountService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
@Service(version = "1.0.0")
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public Boolean decreaseAccount(Account accountDto) {

        Account account = this.accountMapper.selectOne(new QueryWrapper<Account>()
            .eq("user_id",accountDto.getUserId())
        );
        if (account != null){
            account.setAmount(account.getAmount() - accountDto.getAmount());
        }

        return this.accountMapper.updateById(account) > 0;
    }
}
