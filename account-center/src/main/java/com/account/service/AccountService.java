package com.account.service;

import com.account.model.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface AccountService extends IService<Account> {
    /**
     * 扣用户钱
     */
    Boolean decreaseAccount(Account account);
}
