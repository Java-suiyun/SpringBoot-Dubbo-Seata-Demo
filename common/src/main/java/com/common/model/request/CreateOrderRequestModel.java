package com.common.model.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ww
 * @date 2020/6/10 下午1:52
 */
@Data
public class CreateOrderRequestModel implements Serializable {
    private String userId;

    private String commodityCode;

    private String name;

    private Integer count;

    private BigDecimal amount;
}
