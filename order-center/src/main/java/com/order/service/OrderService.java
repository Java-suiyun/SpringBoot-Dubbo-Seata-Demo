package com.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.model.request.CreateOrderRequestModel;
import com.order.model.Order;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface OrderService extends IService<Order> {

    /**
     * 创建订单
     */
    Boolean createOrder(CreateOrderRequestModel model);
}
