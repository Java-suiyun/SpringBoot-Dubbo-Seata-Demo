package com.order.service.impl;

import com.account.model.Account;
import com.account.service.AccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.model.request.CreateOrderRequestModel;
import com.order.mapper.OrderMapper;
import com.order.model.Order;
import com.order.service.OrderService;
import com.storage.model.Storage;
import com.storage.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
@Service(version = "1.0.0")
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Reference(version = "1.0.0")
    private AccountService accountService;

    @Reference(version = "1.0.0")
    private StorageService storageService;


    @GlobalTransactional(timeoutMills = 300000, name = "dubbo-order-seata-example")
    @Override
    public Boolean createOrder(CreateOrderRequestModel model){
        //扣库存
        Storage storage = new Storage();
        storage.setCommodityCode(model.getCommodityCode());
        storage.setCount(model.getCount());
        this.storageService.decreaseStorage(storage);

        //扣减用户账户
        Account account = new Account();
        account.setAmount(model.getAmount().doubleValue());
        account.setUserId(model.getUserId());

        this.accountService.decreaseAccount(account);
        //生成订单号
        Order order = new Order();
        BeanUtils.copyProperties(model,order);
        order.setOrderNo(UUID.randomUUID().toString().replace("-",""));
        order.setAmount(model.getAmount().doubleValue());
        try {
            this.baseMapper.insert(order);
        }catch (Exception e){
            return Boolean.FALSE;
        }

        if (true) {
            throw new RuntimeException("测试抛异常后，分布式事务回滚！");
        }

        return Boolean.TRUE;
    }
}
