package com.order.mapper;

import com.order.model.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ww
 * @since 2020-06-09
 */
public interface OrderMapper extends BaseMapper<Order> {

}
